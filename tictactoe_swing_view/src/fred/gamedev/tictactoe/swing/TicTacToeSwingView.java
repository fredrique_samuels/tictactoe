/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.swing;

import java.awt.Container;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import fred.gamedev.tictactoe.State;
import fred.gamedev.tictactoe.Symbol;
import fred.gamedev.tictactoe.game.TicTacToeView;
import fred.gamedev.tictactoe.game.commands.GameActionCommand;
import fred.gamedev.tictactoe.game.commands.NewGameCommand;
import fred.gamedev.tictactoe.game.commands.QuitGameCommand;

public class TicTacToeSwingView extends JFrame implements TicTacToeView {

	final static Logger LOG = Logger.getLogger(TicTacToeSwingView.class
			.getName());
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static int DIM = 100;
	private final static int SPACE = 10;
	private final static int LABEL_HEIGHT = 30;
	private final int WIDTH = SPACE * 5 + 3 * DIM;
	private final int MENU_HEIGHT = 50;
	private final int HEIGHT = MENU_HEIGHT + SPACE + LABEL_HEIGHT
			+ (3 * (SPACE + DIM));

	private JLabel statusLabel;
	private JButton[] buttons;

	private JMenuBar menuBar;

	private JMenuItem newAction;

	private JMenuItem quitAction;

	public TicTacToeSwingView() {

		Container contentPane = getContentPane();

		configureContentPane(contentPane);
		addMenuItems();
		addStatusLabel(contentPane);
		addGridButtons(contentPane);
	}

	public void run() {
		setVisible(true);
	}

	private void addMenuItems() {
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu fileMenu = new JMenu("File");
		menuBar.add(fileMenu);

		newAction = new JMenuItem("New");
		fileMenu.add(newAction);

		quitAction = new JMenuItem("Quit");
		fileMenu.add(quitAction);
	}

	private void configureContentPane(Container contentPane) {
		setSize(WIDTH, HEIGHT);
		contentPane.setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}

	private void addStatusLabel(Container contentPane) {
		statusLabel = new JLabel("");
		statusLabel.setBounds(20, 0, WIDTH, LABEL_HEIGHT);
		contentPane.add(statusLabel);
	}

	private void addGridButtons(Container pane) {
		buttons = new JButton[9];
		int yPos = LABEL_HEIGHT + SPACE;

		for (int i = 0; i < 9; ++i) {
			int index = i % 3;
			int xPos = (index + 1) * SPACE + index * DIM;

			JButton button = new JButton();
			button.setFont(new Font("Arial", Font.BOLD, 30));
			button.setBounds(xPos, yPos, DIM, DIM);

			buttons[i] = button;
			if (i % 3 == 2) {
				yPos += DIM + SPACE;
			}
			pane.add(button);
		}
	}

	@Override
	public void destroy() {
		System.exit(1);
	}

	@Override
	public void reset() {
		statusLabel.setText("");
	}

	@Override
	public void setActionCommand0(final GameActionCommand action) {
		buttons[0].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setActionCommand1(final GameActionCommand action) {
		buttons[1].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setActionCommand2(final GameActionCommand action) {
		buttons[2].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setActionCommand3(final GameActionCommand action) {
		buttons[3].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setActionCommand4(final GameActionCommand action) {
		buttons[4].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setActionCommand5(final GameActionCommand action) {
		buttons[5].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setActionCommand6(final GameActionCommand action) {
		buttons[6].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setActionCommand7(final GameActionCommand action) {
		buttons[7].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setActionCommand8(final GameActionCommand action) {
		buttons[8].addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setNewCommand(final NewGameCommand action) {
		newAction.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void setQuitCommand(final QuitGameCommand action) {
		quitAction.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				action.execute();
			}
		});
	}

	@Override
	public void draw() {
		statusLabel.setText("Game ended in a Draw!");
	}

	@Override
	public void update(State board) {
		for (int i = 0; i < 9; ++i) {
			Symbol symbol = board.getSymbol(i);
			setCell(i, symbol);
		}
	}

	private void setCell(int i, Symbol symbol) {
		String text = "";
		if (symbol == Symbol.SYMBOL_X) {
			text = "X";
		} else if (symbol == Symbol.SYMBOL_O) {
			text = "O";
		}
		buttons[i].setText(text);
	}

	@Override
	public void winner(int[] combo) {
		LinkedList<Integer> linkedList = new LinkedList<Integer>();
		linkedList.add(combo[0]);
		linkedList.add(combo[1]);
		linkedList.add(combo[2]);

		for (int i = 0; i < 9; ++i) {
			if (linkedList.contains(i)) {
				continue;
			}
			setCell(i, Symbol.SYMBOL_N);
		}

		statusLabel.setText("Winnner!!");
	}

	@Override
	public void setStatus(String status) {
		statusLabel.setText(status);
	}

	@Override
	public void setOpenCells(Symbol symbolX, int[] cells) {
	}

}
