(start)->(Compile Valid move list for PlayerX)->(Request PlayerX to make a move)->(PlayerX makes move)->(Clear valid move list for PlayerX)-><plrXWon>[won]->(Notify PlayerX of win)->(Notify PlayerO of loss)->(end), <plrXWon>-><plrXDraw>[draw]->(1. Notify players of draw game)->(end), <plrXDraw>->(Compile valid move list for PlayerO)->(Request PlayerO to make a move)->(PlayerO makes Move)->(Clear valid move list for PlayerO)-><plrOWon>[won]->(Notify PlayerO of win)->(Notify PlayerX of loss)->(end), <plrOWon>-><plrODraw>[draw]->(2. Notify players of draw game)->(end),<plrODraw>->(Compile Valid move list for PlayerX)


Model Activity Flow for View Notification (Click to enlarge) : Created using http://yuml.me/diagram/nofunky/activity/draw

==== Demo From Site ============
(start)->|a|,|a|->(Make Coffee)->|b|,|a|->(Make Breakfast)->|b|,|b|-><c>[want more coffee]->(Make Coffee),<c>[satisfied]->(end)