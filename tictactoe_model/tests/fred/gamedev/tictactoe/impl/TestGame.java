/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.impl;

import junit.framework.TestCase;
import fred.gamedev.tictactoe.PlayerController;
import fred.gamedev.tictactoe.State;
import fred.gamedev.tictactoe.StateListener;
import fred.gamedev.tictactoe.Player;
import fred.gamedev.tictactoe.Symbol;
import fred.gamedev.tictactoe.TicTacToe;

public class TestGame extends TestCase {
	
	class TestPlayer implements Player {
		boolean win = false;
		boolean lose = false;
		boolean draw = false;
		int[] aiMoves;
		int moveCount;
		private PlayerController controller;

		public TestPlayer(int[] moves) {
			this.aiMoves = moves;
			moveCount = 0;
		}

		@Override
		public void draw() {
			draw = true;
		}

		@Override
		public void lost() {
			lose = true;
		}

		@Override
		public void won() {
			win = true;
		}

		@Override
		public void setController(PlayerController controller) {
			this.controller = controller;
		}

		@Override
		public void inputRequest(State board) {
			if (controller.getActionCount() > 0) {
				int moveindex = aiMoves[moveCount];
				moveCount+=1;
				controller.execute(controller.getAction(moveindex));
			}
		}

		@Override
		public void validMovesChanged() {
		}
	}
	
	public class TestView implements StateListener{
		boolean draw = false;
		boolean winner = false;
		State board = null;
		int[] combo = null;
		
		@Override
		public void draw() {
			draw = true;
		}

		@Override
		public void update(State board) {
			this.board = board;
		}

		@Override
		public void winner(int[] combo) {
			winner = true;
			this.combo = combo;
		}
	}

	public void testGameWinner() {
		/**
		 * This game ends with a board. 
		 * XOX 
		 * OXO 
		 * X
		 */

		/* Always choos the first open cell */
		TestView testView = new TestView();
		int[] moves = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
		TestPlayer playerX = new TestPlayer(moves);
		TestPlayer playerO = new TestPlayer(moves);
		
		TicTacToe game = new TicTacToeImpl(playerX, playerO);
		game.addStateListener(testView);
		game.start();

		assertTrue(playerX.win);
		assertFalse(playerX.lose);
		assertFalse(playerX.draw);

		assertFalse(playerO.win);
		assertTrue(playerO.lose);
		assertFalse(playerO.draw);
		
		State grid = testView.board;
		
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(0));
		assertEquals(Symbol.SYMBOL_O, grid.getSymbol(1));
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(2));
		assertEquals(Symbol.SYMBOL_O, grid.getSymbol(3));
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(4));
		assertEquals(Symbol.SYMBOL_O, grid.getSymbol(5));
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(6));
		assertEquals(Symbol.SYMBOL_N, grid.getSymbol(7));
		assertEquals(Symbol.SYMBOL_N, grid.getSymbol(8));		

		assertNotNull(testView.combo);
		assertEquals(2, testView.combo[0]);
		assertEquals(4, testView.combo[1]);
		assertEquals(6, testView.combo[2]);
		
		assertTrue(testView.winner);
		assertFalse(testView.draw);

		game.destroy();
	}
	
	public void testGameDraw() {
		/**
		 * This game ends with a board. 
		 * XOX 
		 * XXO 
		 * OXO
		 */

		TestView testView = new TestView();
		int[] movesX = new int[] { 0, 1, 1, 1, 0};
		int[] movesO = new int[] { 0, 3, 1, 1};
		TestPlayer playerX = new TestPlayer(movesX);
		TestPlayer playerO = new TestPlayer(movesO);

		TicTacToe game = new TicTacToeImpl(playerX, playerO);
		game.addStateListener(testView);
		game.start();

		assertFalse(playerX.win);
		assertFalse(playerX.lose);
		assertTrue(playerX.draw);

		assertFalse(playerO.win);
		assertFalse(playerO.lose);
		assertTrue(playerO.draw);

		State grid = testView.board;
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(0));
		assertEquals(Symbol.SYMBOL_O, grid.getSymbol(1));
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(2));
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(3));
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(4));
		assertEquals(Symbol.SYMBOL_O, grid.getSymbol(5));
		assertEquals(Symbol.SYMBOL_O, grid.getSymbol(6));
		assertEquals(Symbol.SYMBOL_X, grid.getSymbol(7));
		assertEquals(Symbol.SYMBOL_O, grid.getSymbol(8));
		
		assertTrue(testView.draw);
		assertFalse(testView.winner);
		
		game.destroy();
	}
}
