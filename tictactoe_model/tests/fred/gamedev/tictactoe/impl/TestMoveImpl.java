/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.impl;

import fred.gamedev.tictactoe.Symbol;
import junit.framework.TestCase;

public class TestMoveImpl extends TestCase{
	public void testInit() {
		// Given
		MoveImpl move1 = new MoveImpl(Symbol.SYMBOL_O, 2);
		MoveImpl move2 = new MoveImpl(Symbol.SYMBOL_X, 5);
		
		// Then
		assertEquals(Symbol.SYMBOL_O, move1.getSymbol());
		assertEquals(2, move1.getCell());
		assertEquals(Symbol.SYMBOL_X, move2.getSymbol());
		assertEquals(5, move2.getCell());
	}
	
	public void testEquals() {
		// Given
		MoveImpl move1 = new MoveImpl(Symbol.SYMBOL_O, 2);
		MoveImpl move2 = new MoveImpl(Symbol.SYMBOL_X, 5);
		MoveImpl move3 = new MoveImpl(Symbol.SYMBOL_O, 3);
		MoveImpl move4 = new MoveImpl(Symbol.SYMBOL_X, 2);
		MoveImpl move5 = new MoveImpl(Symbol.SYMBOL_O, 2);
		
		assertFalse(move1.equals(move2));
		assertFalse(move1.equals(move3));
		assertFalse(move1.equals(move4));
		assertTrue(move1.equals(move5));
	}
}
