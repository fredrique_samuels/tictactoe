/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.impl;

import junit.framework.TestCase;
import fred.gamedev.tictactoe.Symbol;

public class TestBoard extends TestCase {
	public void testInit() {
		// Given
		StateImpl board = new StateImpl();

		// Then
		Symbol[] grid = board.getGrid();
		assertEquals(grid[0], Symbol.SYMBOL_N);
		assertEquals(grid[1], Symbol.SYMBOL_N);
		assertEquals(grid[2], Symbol.SYMBOL_N);
		assertEquals(grid[3], Symbol.SYMBOL_N);
		assertEquals(grid[4], Symbol.SYMBOL_N);
		assertEquals(grid[5], Symbol.SYMBOL_N);
		assertEquals(grid[6], Symbol.SYMBOL_N);
		assertEquals(grid[7], Symbol.SYMBOL_N);
		assertEquals(grid[8], Symbol.SYMBOL_N);

		assertEquals(9, board.getOpenCellCount());
		assertEquals(9, board.getValidMoves().length);
		assertNull(board.getWinningCombo());
	}
	
	public void testSet() {
		// Given
		StateImpl board = new StateImpl();

		//When
		board.set(0, Symbol.SYMBOL_O);
		assertEquals(8, board.getOpenCellCount());
		
		board.set(1, Symbol.SYMBOL_X);
		assertEquals(7, board.getOpenCellCount());
		
		board.set(2, Symbol.SYMBOL_O);
		assertEquals(6, board.getOpenCellCount());
		
		board.set(3, Symbol.SYMBOL_O);
		assertEquals(5, board.getOpenCellCount());
		
		board.set(4, Symbol.SYMBOL_X);
		assertEquals(4, board.getOpenCellCount());
		
		board.set(5, Symbol.SYMBOL_O);
		assertEquals(3, board.getOpenCellCount());
		
		board.set(6, Symbol.SYMBOL_O);
		assertEquals(2, board.getOpenCellCount());
		
		board.set(7, Symbol.SYMBOL_X);
		assertEquals(1, board.getOpenCellCount());
		
		board.set(8, Symbol.SYMBOL_X);
		assertEquals(0, board.getOpenCellCount());
		
		// Then
		Symbol[] grid = board.getGrid();
		assertEquals(Symbol.SYMBOL_O, grid[0]);
		assertEquals(Symbol.SYMBOL_X, grid[1]);
		assertEquals(Symbol.SYMBOL_O, grid[2]);
		assertEquals(Symbol.SYMBOL_O, grid[3]);
		assertEquals(Symbol.SYMBOL_X, grid[4]);
		assertEquals(Symbol.SYMBOL_O, grid[5]);
		assertEquals(Symbol.SYMBOL_O, grid[6]);
		assertEquals(Symbol.SYMBOL_X, grid[7]);
		assertEquals(Symbol.SYMBOL_X, grid[8]);
	}

}
