/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.impl;

import fred.gamedev.tictactoe.State;
import fred.gamedev.tictactoe.Symbol;

final class StateImpl implements State{
	private static int[][] WINNING_COMBOS = new int[][] { { 0, 4, 8 },
			{ 2, 4, 6 }, { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 }, { 0, 3, 6 },
			{ 1, 4, 7 }, { 2, 5, 8 } };
	
	private final Symbol[] grid;
	private int[] winningCombo;

	public StateImpl() {
		grid = new Symbol[] { Symbol.SYMBOL_N, Symbol.SYMBOL_N,
				Symbol.SYMBOL_N, Symbol.SYMBOL_N, Symbol.SYMBOL_N,
				Symbol.SYMBOL_N, Symbol.SYMBOL_N, Symbol.SYMBOL_N,
				Symbol.SYMBOL_N };
		winningCombo = null;
	}
	
	public Symbol[] getGrid() {
		Symbol[] result = new Symbol[grid.length];
		for (int i = 0; i < grid.length; ++i) {
			result[i] = grid[i];
		}
		return result;
	}

	public int[] getValidMoves() {
		int openCellCount = getOpenCellCount();

		int[] moves = new int[openCellCount];
		int index = 0;
		for (int i = 0; i < grid.length; ++i) {
			if (grid[i] == Symbol.SYMBOL_N) {
				moves[index] = i;
				++index;
			}
		}
		return moves;
	}

	public int getOpenCellCount() {
		int openCellCount = 0;
		for (Symbol s : grid) {
			if (s == Symbol.SYMBOL_N) {
				++openCellCount;
			}
		}
		return openCellCount;
	}

	public boolean hasWon(Symbol symbol) {
		boolean winner = false;
		for (int[] combo : WINNING_COMBOS) {
			if (grid[combo[0]] == symbol && grid[combo[1]] == symbol
					&& grid[combo[2]] == symbol) {
				winningCombo = combo;
				winner = true;
				break;
			}
		}
		return winner;
	}

	public void reset() {
		for (int i = 0; i < grid.length; ++i) {
			grid[i] = Symbol.SYMBOL_N;
		}
		winningCombo = null;
	}
	
	public int[] getWinningCombo() {
		return winningCombo;
	}

	public void set(int cell, Symbol symbol) {
		grid[cell] = symbol;
	}

	@Override
	public Symbol getSymbol(int cell) {
		return grid[cell];
	}
	
	
}
