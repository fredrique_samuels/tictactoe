/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.impl;

import java.util.LinkedList;
import java.util.List;

import fred.gamedev.tictactoe.Action;
import fred.gamedev.tictactoe.StateListener;
import fred.gamedev.tictactoe.Player;
import fred.gamedev.tictactoe.Symbol;

public class Logic {
	private final List<StateListener> views;
	private Player playerX;
	private Player playerO;
	private ControllerImpl controllerX;
	private ControllerImpl controllerO;
	private StateImpl state;

	public Logic(StateImpl state, Player playerX, Player playerO) {
		views = new LinkedList<StateListener>();
		this.playerX = playerX;
		this.playerO = playerO;
		this.state = state;
		
		controllerX = new ControllerImpl(Symbol.SYMBOL_X, this);
		playerX.setController(controllerX);
		
		controllerO = new ControllerImpl(Symbol.SYMBOL_O, this);
		playerO.setController(controllerO);
	}
	
	public void start() {
		dispatchBoard();
		doTurnX();
	}
	
	private void dispatchBoard() {
		for (StateListener view : views) {
			view.update(state);
		}
	}

	private void doTurnX() {
		controllerX.setMoves(state.getValidMoves());
		playerX.validMovesChanged();
		playerX.inputRequest(state);
		dispatchBoard();
	}

	private void doTurnO() {
		controllerO.setMoves(state.getValidMoves());
		playerO.validMovesChanged();
		playerO.inputRequest(state);
		dispatchBoard();
	}

	synchronized void doMove(Action move) {
		controllerX.clearMoves();
		controllerO.clearMoves();
		playerX.validMovesChanged();
		playerO.validMovesChanged();

		Symbol symbol = move.getSymbol();
		state.set(move.getCell(), symbol);
		dispatchBoard();
		
		if (symbol == Symbol.SYMBOL_X) {
			if (state.hasWon(symbol))
				playerXWins(symbol);
			else if (state.getOpenCellCount() == 0)
				drawGame();
			else
				doTurnO();
		} else {
			if (state.hasWon(symbol))
				playerOWins(symbol);
			else if (state.getOpenCellCount() == 0)
				drawGame();
			else
				doTurnX();
		}
	}

	private void drawGame() {
		playerO.draw();
		playerX.draw();
		dispatchGameDraw();
	}

	private void dispatchGameDraw() {
		for (StateListener view : views) {
			view.draw();
		}
	}

	private void dispatchWinner() {
		for (StateListener view : views) {
			view.winner(state.getWinningCombo());
		}
	}

	private void playerOWins(Symbol symbol) {
		playerO.won();
		playerX.lost();
		dispatchWinner();
	}

	private void playerXWins(Symbol symbol) {
		playerX.won();
		playerO.lost();
		dispatchWinner();
	}
	
	public void addStateListener(StateListener view) {
		if (!views.contains(view))
			views.add(view);
	}

	public void removeStateListener(StateListener view) {
		if (views.contains(view))
			views.remove(view);
	}

	public void destroy() {
		controllerO.destroy();
		controllerX.destroy();
	}
}
