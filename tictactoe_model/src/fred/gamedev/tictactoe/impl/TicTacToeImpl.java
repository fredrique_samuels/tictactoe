/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.impl;

import fred.gamedev.tictactoe.Action;
import fred.gamedev.tictactoe.StateListener;
import fred.gamedev.tictactoe.Player;
import fred.gamedev.tictactoe.TicTacToe;

public final class TicTacToeImpl implements TicTacToe {

	public static final Action[] EMPTY_MOVES = new Action[0];
	
	private StateImpl state;
	private Logic logic;

	public TicTacToeImpl(Player playerX, Player playerO) {
		state = new StateImpl();
		logic = new Logic(state, playerX, playerO);
	}

	
	public void reset() {
		state.reset(); 
	}

	public void start() {
		logic.start();
	}	

	@Override
	public void addStateListener(StateListener view) {
		logic.addStateListener(view);
	}

	@Override
	public void removeStateListener(StateListener view) {
		logic.removeStateListener(view);
	}

	@Override
	public void destroy() {
		logic.destroy();
	}
}
