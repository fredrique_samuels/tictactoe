/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.impl;

import fred.gamedev.tictactoe.Action;
import fred.gamedev.tictactoe.Symbol;

final class MoveImpl implements Action{
	private final Symbol symbol;
	private final int cell;

	public MoveImpl(Symbol symbol, int cell) {
		this.symbol = symbol;
		this.cell = cell;
	}

	@Override
	public int getCell() {
		return cell;
	}

	@Override
	public Symbol getSymbol() {
		return symbol;
	}
	
	@Override
	public boolean equals(Object arg0) {
		if(arg0 instanceof Action){
			Action move = (Action)arg0;
			return move.getCell()==cell && move.getSymbol()==symbol;
		}
		return false;
	}
	
}
