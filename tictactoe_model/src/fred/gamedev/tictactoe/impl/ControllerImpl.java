/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.impl;

import java.util.LinkedList;
import java.util.List;

import fred.gamedev.tictactoe.Action;
import fred.gamedev.tictactoe.Symbol;

public class ControllerImpl implements fred.gamedev.tictactoe.PlayerController {

	private final List<Action> moves;
	private Logic logic;
	private final Symbol symbol;
	
	public ControllerImpl(Symbol symbol, Logic logic) {
		moves = new LinkedList<Action>();
		this.logic = logic;
		this.symbol = symbol;
	}
	
	@Override
	public synchronized void execute(Action move) {
		if (moves.contains(move)) {
			logic.doMove(move);
		}
	}

	@Override
	public Action getAction(int index) {
		return moves.get(index);
	}

	@Override
	public int getActionCount() {
		return moves.size();
	}

	public void clearMoves() {
		moves.clear();
	}

	public void setMoves(int[] validMoves) {
		moves.clear();
		for (int index : validMoves) {
			moves.add(new MoveImpl(symbol, index));
		}
	}

	public void destroy() {
		logic = null;
	}
}
