package fred.gamedev.tictactoe;

import fred.gamedev.tictactoe.impl.TicTacToeImpl;

public class GameFactory {
	public static TicTacToe create(Player playerX, Player playerO) {
		return new TicTacToeImpl(playerX, playerO);
	}
	
	private GameFactory() {
	}
}
