/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.game.players;

import fred.gamedev.tictactoe.Action;
import fred.gamedev.tictactoe.State;
import fred.gamedev.tictactoe.game.ViewController;

public class HumanPlayer extends BasePlayer {

	private ViewController viewController;

	public HumanPlayer(ViewController viewController) {
		super();
		this.viewController = viewController;
	}

	@Override
	public void draw() {
	}

	@Override
	public void inputRequest(State board) {
		viewController.setStatus("Your Turn!");
	}

	public int[] getOpenCells() {
		int actionCount = playerController.getActionCount();
		int[] cells = new int[actionCount]; 
		for (int i=0;i<actionCount;++i) {
			Action action = playerController.getAction(i);
			cells[i] = action.getCell();
		}
		return cells;
	}

	@Override
	public void lost() {

	}

	@Override
	public void won() {

	}

	public void doMove(int cell) {
		for (int i = 0; i < playerController.getActionCount(); ++i) {
			Action action  = playerController.getAction(i);
			if(action.getCell()==cell) {
				playerController.execute(action);
			}
		}
	}

	@Override
	public void validMovesChanged() {
		int[] cells = getOpenCells();
		viewController.setOpenCells(cells);
	}
}
