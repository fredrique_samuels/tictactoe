/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.game;

import fred.gamedev.tictactoe.StateListener;
import fred.gamedev.tictactoe.Symbol;
import fred.gamedev.tictactoe.game.commands.GameActionCommand;
import fred.gamedev.tictactoe.game.commands.NewGameCommand;
import fred.gamedev.tictactoe.game.commands.QuitGameCommand;

public interface TicTacToeView extends StateListener{
	public void reset();
	public void destroy();

	public void setStatus(String status);
	public void setOpenCells(Symbol symbolX, int[] cells);
	
	public void setNewCommand(NewGameCommand action);
	public void setQuitCommand(QuitGameCommand action);
	public void setActionCommand0(GameActionCommand action);
	public void setActionCommand1(GameActionCommand action);
	public void setActionCommand2(GameActionCommand action);
	public void setActionCommand3(GameActionCommand action);
	public void setActionCommand4(GameActionCommand action);
	public void setActionCommand5(GameActionCommand action);
	public void setActionCommand6(GameActionCommand action);
	public void setActionCommand7(GameActionCommand action);
	public void setActionCommand8(GameActionCommand action);
}

