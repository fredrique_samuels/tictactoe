/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.game;

import fred.gamedev.tictactoe.Symbol;
import fred.gamedev.tictactoe.game.commands.GameActionCommand;
import fred.gamedev.tictactoe.game.commands.NewGameCommand;
import fred.gamedev.tictactoe.game.commands.QuitGameCommand;
import fred.gamedev.tictactoe.game.players.ComputerPlayer;
import fred.gamedev.tictactoe.game.players.HumanPlayer;
import fred.gamedev.tictactoe.impl.TicTacToeImpl;

public class ViewController {
	
	private TicTacToeImpl game;
	private TicTacToeView view;
	private HumanPlayer humanPlayer;
	private ComputerPlayer computerPlayer;

	public ViewController(TicTacToeView view) {
		humanPlayer = new HumanPlayer(this);
		computerPlayer = new ComputerPlayer();
		
		this.view = view;
		this.game = new TicTacToeImpl(humanPlayer, computerPlayer);
		this.game.addStateListener(view);
		
		view.setNewCommand(new NewGameCommand(this));
		view.setQuitCommand(new QuitGameCommand(this));
		view.setActionCommand0(new GameActionCommand(0, this));
		view.setActionCommand1(new GameActionCommand(1, this));
		view.setActionCommand2(new GameActionCommand(2, this));
		view.setActionCommand3(new GameActionCommand(3, this));
		view.setActionCommand4(new GameActionCommand(4, this));
		view.setActionCommand5(new GameActionCommand(5, this));
		view.setActionCommand6(new GameActionCommand(6, this));
		view.setActionCommand7(new GameActionCommand(7, this));
		view.setActionCommand8(new GameActionCommand(8, this));
		
		this.game.start();
	}

	public void newGame() {
		view.reset();
		game.reset();
		game.start();
	}

	public void quit() {
		view.destroy();
	}
	
	public void makeMove(int cell) {
		humanPlayer.doMove(cell);
	}

	public void setStatus(String string) {
		view.setStatus(string);
	}

	public void setOpenCells(int[] cells) {
		view.setOpenCells(Symbol.SYMBOL_X, cells);
	}
}
