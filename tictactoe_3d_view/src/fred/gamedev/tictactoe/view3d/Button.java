/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.view3d;

import iv3d.base.Color;
import iv3d.base.Point;
import iv3d.base.exceptions.GraphicsException;

import com.gamedev.cairo.widgets.Font;
import com.gamedev.cairo.widgets.FontStyle;
import com.gamedev.cairo.widgets.Insets;
import com.gamedev.cairo.widgets.StyleSheet;
import com.gamedev.cairo.widgets.TextAlignX;
import com.gamedev.cairo.widgets.TextFillRule;
import com.gamedev.cairo.widgets.WindowWidget;
import com.gamedev.cairo4j.CairoFontSlant;
import com.gamedev.cairo4j.CairoFontWeight;

public class Button extends WindowWidget{
	
	public Button() {
		super();
		setSize(128, 64);
		StyleSheet styleSheet = getStyleSheet();
		setBackgroundImages(styleSheet);
		
		Font font = new Font("Arial", CairoFontSlant.CAIRO_FONT_SLANT_NORMAL, 
				CairoFontWeight.CAIRO_FONT_WEIGHT_BOLD, 25);
		FontStyle fontStyle1 = new FontStyle(Color.WHITE, new Point(), 
				TextFillRule.SCALE_IF_EXCEED ,TextAlignX.ALIGN_CENTER);
		FontStyle fontStyle2 = new FontStyle(new Color(125), new Point(2, 2), fontStyle1);
		
		styleSheet.setFont(font);
		styleSheet.setFontStyles(fontStyle2, fontStyle1);
	}

	private void setBackgroundImages(StyleSheet styleSheet) {
		styleSheet.setBackgroundColor(new Color(0,0,0,0));
		try {
			styleSheet.setImage("/images/button3_.png");
//			styleSheet.setImagePressed("/images/button3.png");
//			styleSheet.setImageHover("/images/button6.png");
		} catch (GraphicsException e) {
			e.printStackTrace();
		}
		styleSheet.setImageInsets(new Insets(5, 5, 5, 5));
	}
	
}
