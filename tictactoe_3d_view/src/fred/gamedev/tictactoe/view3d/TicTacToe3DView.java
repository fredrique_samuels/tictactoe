/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.view3d;

import iv3d.base.events.Event;
import iv3d.base.events.EventListener;
import iv3d.base.events.EventType;
import iv3d.base.exceptions.GraphicsException;
import iv3d.display.Window;
import iv3d.graphics.SceneManager;

import java.util.logging.Logger;

import fred.gamedev.tictactoe.State;
import fred.gamedev.tictactoe.Symbol;
import fred.gamedev.tictactoe.game.TicTacToeView;
import fred.gamedev.tictactoe.game.commands.GameActionCommand;
import fred.gamedev.tictactoe.game.commands.NewGameCommand;
import fred.gamedev.tictactoe.game.commands.QuitGameCommand;

public final class TicTacToe3DView implements TicTacToeView {

	final static Logger LOG = Logger.getLogger(TicTacToe3DView.class
			.getName());
	
	HudScene hudScene;
	TicTacToeScene tttScene;

	private Window window;
	

	public TicTacToe3DView() throws GraphicsException {
		int width = 1024;
		int height = 768;
		String caption = "Tic-Tac-Toe 3D";
		window = new Window(caption, width, height);

		hudScene = new HudScene();
		SceneManager.add(hudScene);
		hudScene.show();
		
		tttScene = new TicTacToeScene();
		SceneManager.add(tttScene);
		tttScene.show();
	}

	public void run() {
//		window.fullscreen();
		window.run();
	}


	@Override
	public void destroy() {
		window.exit();
	}

	@Override
	public void reset() {
		tttScene.reset();
	}

	@Override
	public void setActionCommand0(GameActionCommand action) {
		setMarkAction(0, action);
	}

	private void setMarkAction(final int mark, final GameActionCommand action) {
		tttScene.addEventListener(mark, new EventListener() {
			
			@Override
			public void onEventStateChanged(Event event) {
				if(event.getEventType()==EventType.MOUSE_CLICK) {
					action.execute();
				}
			}
		});
	}

	@Override
	public void setActionCommand1(final GameActionCommand action) {
		setMarkAction(1, action);
	}

	@Override
	public void setActionCommand2(GameActionCommand action) {
		setMarkAction(2, action);
	}

	@Override
	public void setActionCommand3(GameActionCommand action) {
		setMarkAction(3, action);
	}

	@Override
	public void setActionCommand4(GameActionCommand action) {
		setMarkAction(4, action);
	}

	@Override
	public void setActionCommand5(GameActionCommand action) {
		setMarkAction(5, action);
	}

	@Override
	public void setActionCommand6(GameActionCommand action) {
		setMarkAction(6, action);
	}

	@Override
	public void setActionCommand7(GameActionCommand action) {
		setMarkAction(7, action);
	}

	@Override
	public void setActionCommand8(GameActionCommand action) {
		setMarkAction(8, action);
	}

	@Override
	public void setNewCommand(final NewGameCommand action) {
		hudScene.setNewGameEventListener(new EventListener() {
			
			@Override
			public void onEventStateChanged(Event event) {
				if(event.getEventType()==EventType.MOUSE_CLICK) {
					action.execute();
				}
			}
		});
	}

	@Override
	public void setQuitCommand(final QuitGameCommand action) {
		hudScene.setQuitGameEventListener(new EventListener() {
			
			@Override
			public void onEventStateChanged(Event event) {
				if(event.getEventType()==EventType.MOUSE_CLICK) {
					action.execute();
				}
			}
		});
	}

	@Override
	public void setStatus(String status) {
		hudScene.setStatus(status);
	}

	@Override
	public void draw() {
	}

	@Override
	public void update(State board) {
		for(int i=0;i<9;++i) {
			tttScene.markLocation(i, board.getSymbol(i));
		}
	}

	@Override
	public void winner(int[] combo) {
		tttScene.setWinningCombo(combo);
	}

	@Override
	public void setOpenCells(Symbol symbol, int[] cells) {
		tttScene.setActiveSymbol(symbol);
		tttScene.setOpenCells(cells);
	}
}
