/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.view3d;

import iv3d.base.events.EventListener;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.GraphicsContext;
import iv3d.graphics.Model;
import iv3d.graphics.OrthoSettings;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.Shader;
import iv3d.graphics.Camera.CameraMode;
import iv3d.graphics.models.PlaneModel;
import iv3d.graphics.widgets.PlaneToWidgetEventTransformer;
import iv3d.graphics.widgets.WidgetImageSource;

public class HudScene extends Scene {
	Model buttonModel;
	Shader buttonShader;
	private Button quitGameButton;
	private Button newGameButton;
	
	public HudScene() throws GraphicsException {
		super(1);

		int screenHeight = GraphicsContext.getInstance().getScreenHeight();
		int screenWidth = GraphicsContext.getInstance().getScreenWidth();

		
		Camera camera = new Camera();
		camera.setMode(CameraMode.ORTHO);
		camera.setOrthoSettings(new OrthoSettings(0, screenHeight, screenWidth, 0));
		camera.setPosition(new Vector3(0, 0, 2));
		addCamera(camera);
		
		buttonShader = CairoShader.create();
		
		buttonModel = new PlaneModel().setShader(buttonShader);
		getContentManager().addModel(buttonModel);

		newGameButton = createButton(screenHeight, screenWidth, 0, "New");
		quitGameButton = createButton(screenHeight, screenWidth, 1, "Exit");
		
	}

	private Button createButton(float screenHeight, float screenWidth, int offset, String text) {
		Button button = new Button();
		button.setText(text);
		WidgetImageSource imageSource = new WidgetImageSource(button);
		
		float buttonHeight = 40;
		float buttonWidth = buttonHeight*2;
		float space = 10;
		
		Entity entity = new Entity();
		entity.setScale(buttonWidth, buttonHeight, 1);
		entity.setPosition(space+(buttonWidth*.5f) + ((space+buttonWidth) * offset), screenHeight-buttonHeight, 0);
		entity.enableEvents();
		entity.addEventListener(new PlaneToWidgetEventTransformer(button));
		entity.setTexture(imageSource);
		buttonModel.addEntity(entity);
		return button;
	}
	
	/**
	 * Application Entry.
	 * @throws GraphicsException 
	 */
	public static void main(String[] args) throws GraphicsException {
		
		/**Create a new window.*/
		int width = 640;
		int height = 480;
		String caption = "HUD Demo";
		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		HudScene graphicsDemo = new HudScene();
		SceneManager.add(graphicsDemo);
		graphicsDemo.show();
		
		/** Start the application. */
//		window.fullscreen();
		window.run();
	}

	public void setNewGameEventListener(EventListener listener) {
		newGameButton.addEventListener(listener);
	}
	
	public void setQuitGameEventListener(EventListener listener) {
		quitGameButton.addEventListener(listener);
	}

	public void setStatus(String status) {
//		statusLabel.setText(status); 
	}
}
