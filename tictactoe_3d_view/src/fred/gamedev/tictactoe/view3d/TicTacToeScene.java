/**
 * Copyright (C) 2014 Fredrique. Samuels.
 * All rights reserved.  Email: fredriquesamuels@gmail.com
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of EITHER:
 *   (1) The GNU Lesser General Public License as published by the Free
 *       Software Foundation; either version 2.1 of the License, or (at
 *       your option) any later version. The text of the GNU Lesser
 *       General Public License is included with this library in the
 *       file LICENSE.TXT.
 *   (2) The BSD-style license that is included with this library in
 *       the file LICENSE-BSD.TXT.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the files
 * LICENSE.TXT and LICENSE-BSD.TXT for more details.
 *
 * @Author: Fredrique Samuels fredriquesamuels@gmail.com
 */

package fred.gamedev.tictactoe.view3d;

import fred.gamedev.tictactoe.Symbol;
import iv3d.base.Color;
import iv3d.base.animation.AnimationCalculator;
import iv3d.base.animation.AnimationManager;
import iv3d.base.animation.LinearVector3Animation;
import iv3d.base.events.Event;
import iv3d.base.events.EventListener;
import iv3d.base.events.EventType;
import iv3d.base.exceptions.GraphicsException;
import iv3d.base.math.Vector3;
import iv3d.display.Window;
import iv3d.graphics.Camera;
import iv3d.graphics.Entity;
import iv3d.graphics.Light;
import iv3d.graphics.Material;
import iv3d.graphics.Model;
import iv3d.graphics.Scene;
import iv3d.graphics.SceneManager;
import iv3d.graphics.Shader;
import iv3d.graphics.models.PlaneModel;
import iv3d.graphics.models.model3ds.Model3DSLoader;
import iv3d.graphics.shaders.ShaderProperty;
import iv3d.graphics.shaders.ShaderRegistry;
import iv3d.graphics.shaders.store.SHADER_C;
import iv3d.graphics.shaders.store.SHADER_C_1L_P;

import java.util.ArrayList;

public class TicTacToeScene extends Scene {
	
	private final Model modelO;
	private final Model modelX;
	private final Model modelCylinder;
	private float BLOCK_SIZE = 2f;
	private float DIV_LENGTH = 3f*BLOCK_SIZE + 1.0f;

	private Color gridColor = Color.RED;
	private float gridHeight = 1f;
	private float gridRadius = .3f;
	float gridOffset = BLOCK_SIZE/1.9f;
	
	Entity comboEntity = new Entity();
	private Color comboColor = Color.GREY;
	
	private Color markXColor = Color.BLUE;
	private Color markOColor = Color.GREEN;
	
	private Camera camera;
	private float cameraHeight = 6f;
	private float cameraRadius = 3f;
	private Vector3 cameraPosition = new Vector3(cameraRadius, cameraHeight, cameraRadius);
	
	private Vector3 lightPosition = new Vector3(6, 11, -2).multiply(1.2f);

	private Mark[] marks;
	private Entity[] eventPlates;
	private Mark blinkEntity;
	
	
	float markSize = BLOCK_SIZE;
	Vector3[] markPositions = new Vector3[] {
			new Vector3(BLOCK_SIZE,gridHeight,BLOCK_SIZE),
			new Vector3(0,gridHeight,BLOCK_SIZE),
			new Vector3(-BLOCK_SIZE,gridHeight,BLOCK_SIZE),
			new Vector3(BLOCK_SIZE,gridHeight,0),
			new Vector3(0,gridHeight,0),
			new Vector3(-BLOCK_SIZE,gridHeight,0),
			new Vector3(BLOCK_SIZE,gridHeight,-BLOCK_SIZE),
			new Vector3(0,gridHeight,-BLOCK_SIZE),
			new Vector3(-BLOCK_SIZE,gridHeight,-BLOCK_SIZE),
	};
	
	private Symbol activeSymbol = Symbol.SYMBOL_N;
	private ArrayList<Integer> openCells;  
	
	public final void setActiveSymbol(Symbol activeSymbol) {
		this.activeSymbol = activeSymbol;
	}

	public TicTacToeScene() throws GraphicsException {
		super();
		camera = new Camera();
		camera.setPosition(cameraPosition);
		
		addCamera(camera);
		enableCameraAnimation();

		createLight();
		createFloor();
		
		modelCylinder = createModelCylinder();
		modelO = createModelO();
		modelX = createModelX();
		
		marks = new Mark[9];
		setupMarks();
		createGrid();
		
		eventPlates = new Entity[9];
		createEventPlates();
		
		blinkEntity = createMark(new Vector3());
		
		openCells = new ArrayList<Integer>();
	}
	
	public void reset() {
		setOpenCells(new int[0]);
		setWinningCombo(new int[0]);
		clearGrid();
	}
	
	public void addEventListener (int markIndex, EventListener listener) {
		eventPlates[markIndex].addEventListener(listener);
	}
	
	public void setOpenCells(int[] cells) {
		stopBlink();
		openCells.clear();
		for(int i:cells){
			openCells.add(i);
		}
	}
	
	public void setWinningCombo(int combo[]) {
		float comboHeight = gridHeight + .2f;
		comboEntity.setScale(DIV_LENGTH, gridRadius, gridRadius);
		comboEntity.enableCastShadow();
		comboEntity.setProperty(ShaderProperty.PROPERTY_FLOAT_ALPHA, 1.0f);
		comboEntity.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
		comboEntity.setProperty(ShaderProperty.PROPERTY_COLOR, comboColor);
		modelCylinder.removeEntity(comboEntity);
		
		
		if(isInCombo(combo, 0) && isInCombo(combo, 3) && isInCombo(combo, 6)){
			modelCylinder.addEntity(comboEntity);
			comboEntity.setRotation(0,90,0);
			comboEntity.setPosition(DIV_LENGTH/4, comboHeight, DIV_LENGTH/2);
		}else if(isInCombo(combo, 1) && isInCombo(combo, 4) && isInCombo(combo, 7)){
			modelCylinder.addEntity(comboEntity);
			comboEntity.setRotation(0,90,0);
			comboEntity.setPosition(0, comboHeight, DIV_LENGTH/2);
		}else if(isInCombo(combo, 2) && isInCombo(combo, 5) && isInCombo(combo, 8)){
			modelCylinder.addEntity(comboEntity);
			comboEntity.setRotation(0,90,0);
			comboEntity.setPosition(-DIV_LENGTH/4, comboHeight, DIV_LENGTH/2);
		}else if(isInCombo(combo, 0) && isInCombo(combo, 1) && isInCombo(combo, 2)){
			modelCylinder.addEntity(comboEntity);
			comboEntity.setRotation(0,0,0);
			comboEntity.setPosition(-DIV_LENGTH/2, comboHeight, DIV_LENGTH/4);
		}else if(isInCombo(combo, 3) && isInCombo(combo, 4) && isInCombo(combo, 5)){
			modelCylinder.addEntity(comboEntity);
			comboEntity.setRotation(0,0,0);
			comboEntity.setPosition(-DIV_LENGTH/2, comboHeight, 0);
		}else if(isInCombo(combo, 6) && isInCombo(combo, 7) && isInCombo(combo, 8)){
			modelCylinder.addEntity(comboEntity);
			comboEntity.setRotation(0,0,0);
			comboEntity.setPosition(-DIV_LENGTH/2, comboHeight, -DIV_LENGTH/4);
		}else if(isInCombo(combo, 0) && isInCombo(combo, 4) && isInCombo(combo, 8)){
			modelCylinder.addEntity(comboEntity);
			comboEntity.setRotation(0,-225,0);
			comboEntity.setPosition(new Vector3(DIV_LENGTH/4, comboHeight, DIV_LENGTH/2)
				.add(new Vector3(.875f, 0, -.875f)));
		} else if(isInCombo(combo, 2) && isInCombo(combo, 4) && isInCombo(combo, 6)){
			modelCylinder.addEntity(comboEntity);
			comboEntity.setRotation(0,45,0);
			comboEntity.setPosition(new Vector3(-DIV_LENGTH/4, comboHeight, DIV_LENGTH/2)
				.add(new Vector3(-.875f, 0, -.875f))
			);
		}
	}

	private boolean isInCombo(int[] combo, int i) {
		boolean found = false;
		for (int j:combo) {
			if(j==i) {
				found = true;
				break;
			}
		}
		return found;
	}

	private void createEventPlates() {
		Shader shader = ShaderRegistry.getShader(SHADER_C.ID);
		Model model = new PlaneModel().setShader(shader);
		getContentManager().addModel(model);
		
		
		for (int i=0;i<9;i++) {
			Vector3 position = markPositions[i];
			eventPlates[i] = new Entity();
			eventPlates[i].enableEvents();
			final int index = i;
			eventPlates[i].addEventListener(new EventListener() {
				
				int markIndex = index;
				
				@Override
				public void onEventStateChanged(Event event) {
					if(activeSymbol==Symbol.SYMBOL_N) {
						return;
					}
					if(!openCells.contains(index)) {
						return;
					}
					if(event.getEventType()==EventType.MOUSE_ENTER) {
						startBlink(markIndex, activeSymbol);
					} else if(event.getEventType()==EventType.MOUSE_EXIT) {
						stopBlink();
					}
				}
			});
			eventPlates[i].setPosition(position);
			eventPlates[i].setRotation(-90,0,0);
			eventPlates[i].setProperty(SHADER_C_1L_P.PROPERTY_COLOR, new Color(0,0));
			eventPlates[i].setScale(markSize, markSize, markSize);
			model.addEntity(eventPlates[i]);
		}
	}

	public void startBlink(int i, Symbol symbol) {
		if(symbol==Symbol.SYMBOL_X){
			modelX.addEntity(blinkEntity);
		} else if (symbol==Symbol.SYMBOL_O) {
			modelO.addEntity(blinkEntity);
		}
		blinkEntity.setPosition(markPositions[i]);
		blinkEntity.startBlink();
	}
	
	public void stopBlink() {
		blinkEntity.stopBlink();
		modelO.removeEntity(blinkEntity);
		modelX.removeEntity(blinkEntity);
	}

	public void markLocation(int i, Symbol symbol) {
		Mark mark = marks[i];
		if(symbol==Symbol.SYMBOL_X){
			markX(i);
		} else if (symbol==Symbol.SYMBOL_O){
			markO(i);
		} else {
			modelX.removeEntity(mark);
			modelO.removeEntity(mark);
		}
	}

	public void enableCameraAnimation() throws GraphicsException {
		LinearVector3Animation animator = new LinearVector3Animation(camera, 
				"setPosition", new Vector3(), new Vector3(), 12000, 0, true);
		
		animator.setAnimationCalculator(new AnimationCalculator<Vector3>() {
			
			@Override
			public Vector3 calculate(float progress, Vector3 startValue,
					Vector3 endValue) {
				float x = (float) (Math.sin(6.28*progress)*cameraRadius);
				float y = cameraHeight;
				float z = (float) (Math.cos(6.28*progress)*cameraRadius);
				return new Vector3(x, y, z);
			}
		});
		AnimationManager.registerAnimation(animator);		
	}

	private void setupMarks() {
		for (int i=0;i<9;i++) {
			Vector3 position = markPositions[i];
			Mark mark = createMark(position);
			marks[i] = mark;
		}
	}

	private Mark createMark(Vector3 position) {
		Mark mark = new Mark();
		setupMark(mark, position);
		return mark;
	}

	private void setupMark(Mark mark, Vector3 position) {
		mark.setScale(markSize, markSize, markSize);
		mark.setPosition(position);
		mark.enableCastShadow();
		mark.setProperty(ShaderProperty.PROPERTY_COLOR, Color.WHITE);
		mark.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
		mark.setProperty("alpha", 1.0f);
	}
	
	public void markO(int index) {
		Entity entity = marks[index];
		entity.setProperty(SHADER_C_1L_P.PROPERTY_COLOR, markOColor);
		entity.setProperty(SHADER_C_1L_P.PROPERTY_FLOAT_ALPHA, 1.0f);
		entity.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
		entity.enableCastShadow();
		modelO.addEntity(entity);
		modelX.removeEntity(entity);
	}
	
	public void markX(int index) {
		Entity entity = marks[index];
		entity.setProperty(SHADER_C_1L_P.PROPERTY_COLOR, markXColor);
		entity.setProperty(SHADER_C_1L_P.PROPERTY_FLOAT_ALPHA, 1.0f);
		entity.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
		entity.enableCastShadow();
		modelX.addEntity(entity);
		modelO.removeEntity(entity);
	}
	
	public void clearGrid() {
		modelX.removeAll();
		modelO.removeAll();
	}

	private void createGrid() {
		{
			Entity entity = new Entity();
			entity.setScale(DIV_LENGTH, gridRadius, gridRadius);
			entity.setPosition(-DIV_LENGTH/2, gridHeight, gridOffset);
			entity.setProperty(ShaderProperty.PROPERTY_COLOR, gridColor);
			entity.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
			entity.setProperty("alpha", 1.0f);
			entity.enableCastShadow();
			modelCylinder.addEntity(entity);
		}
		{
			Entity entity = new Entity();
			entity.setScale(DIV_LENGTH, gridRadius, gridRadius);
			entity.setPosition(-DIV_LENGTH/2, gridHeight, -gridOffset);
			entity.setProperty(ShaderProperty.PROPERTY_COLOR, gridColor);
			entity.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
			entity.setProperty("alpha", 1.0f);
			entity.enableCastShadow();
			modelCylinder.addEntity(entity);
		}
		
		{
			Entity entity = new Entity();
			entity.setScale(DIV_LENGTH, gridRadius, gridRadius);
			entity.setRotation(0,90,0);
			entity.setPosition(-gridOffset, gridHeight, DIV_LENGTH/2);
			entity.enableCastShadow();
			entity.setProperty(ShaderProperty.PROPERTY_COLOR, gridColor);
			entity.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
			entity.setProperty("alpha", 1.0f);
			modelCylinder.addEntity(entity);
		}
		
		{
			Entity entity = new Entity();
			entity.setScale(DIV_LENGTH, gridRadius, gridRadius);
			entity.setRotation(0,90,0);
			entity.setPosition(gridOffset, gridHeight, DIV_LENGTH/2);
			entity.setProperty(ShaderProperty.PROPERTY_COLOR, gridColor);
			entity.setProperty(ShaderProperty.PROPERTY_USE_LIGHT, Shader.TRUE);
			entity.setProperty("alpha", 1.0f);
			entity.enableCastShadow();
			modelCylinder.addEntity(entity);
		}
	}
	
	private Model createModelO() throws GraphicsException {
		Model model = Model3DSLoader.load("/models/torus.3ds");
		String name = model.getMaterialNames().toArray(new String[0])[0];
		Material material = model.getMaterial(name);
		material.setSpecular(Color.WHITE);
		material.setDiffuse(Color.WHITE);
		getContentManager().addModel(model);
		return model;
	}
	
	private Model createModelX() throws GraphicsException {
		Model model = Model3DSLoader.load("/models/x.3ds");
		String name = model.getMaterialNames().toArray(new String[0])[0];
		Material material = model.getMaterial(name);
		material.setSpecular(Color.WHITE);
		material.setDiffuse(Color.WHITE);
		getContentManager().addModel(model);
		return model;
	}
	
	private Model createModelCylinder() throws GraphicsException {
		Model model = Model3DSLoader.load("/models/cyl_1x1x1.3ds");
		getContentManager().addModel(model);
		return model;
	}

	private void createLight() {
		Light light = new Light();
		light.setAngle(35);
		light.setPosition(lightPosition);
		light.enableShadow();
		light.setSpecular(Color.WHITE);
		getContentManager().addLight(light);
	}

	private void createFloor() throws GraphicsException {
		/** Create the image. */
		Entity floor = new Entity();
		floor.setRotation(-90, 0, 0).setScale(30, 30, 1);
		floor.setProperty(ShaderProperty.PROPERTY_COLOR, Color.WHITE);
		floor.enableReceiveShadows();
		
		Model model = new PlaneModel()
			.addEntity(floor);
		getContentManager().addModel(model);
	}
	
	/**
	 * Application Entry.
	 * @throws GraphicsException 
	 */
	public static void main(String[] args) throws GraphicsException {
		
		/**Create a new window.*/
		int width = 1024;
		int height = 768;
		String caption = "TTT 3D Test";
		Window window = new Window(caption, width, height);

		/** Create the demo scene. */
		TicTacToeScene scene = new TicTacToeScene();
//		scene.markLocation(0, Symbol.SYMBOL_X);
//		scene.markLocation(1, Symbol.SYMBOL_X);
		scene.markLocation(2, Symbol.SYMBOL_X);
//		scene.markLocation(3, Symbol.SYMBOL_X);
		scene.markLocation(4, Symbol.SYMBOL_X);
//		scene.markLocation(5, Symbol.SYMBOL_X);
		scene.markLocation(6, Symbol.SYMBOL_X);
//		scene.markLocation(7, Symbol.SYMBOL_X);
//		scene.markLocation(8, Symbol.SYMBOL_X);
//		graphicsDemo.setOpenCells(new int[]{1, 4, 5, 6, 7});
		scene.setActiveSymbol(Symbol.SYMBOL_O);
		
		scene.setWinningCombo(new int[]{2, 4, 6});
		
		
		SceneManager.add(scene);
		scene.show();
		
		/** Start the application. */
//		window.fullscreen();
		window.run();
	}
}
