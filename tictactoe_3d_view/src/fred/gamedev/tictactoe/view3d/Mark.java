package fred.gamedev.tictactoe.view3d;

import iv3d.base.animation.AnimationCalculator;
import iv3d.base.animation.AnimationManager;
import iv3d.base.animation.FloatAnimation;
import iv3d.base.exceptions.GraphicsException;
import iv3d.graphics.Entity;

public class Mark extends Entity {
	
	FloatAnimation blinkAnimation;
	
	public Mark() {
		super();
		
		try {
			blinkAnimation = new FloatAnimation(this, "setAlpha", 0f, 1.0f, 2000, 0, true);
			blinkAnimation.setAnimationCalculator(new AnimationCalculator<Float>() {
				
				@Override
				public Float calculate(float progress, Float startValue, Float endValue) {
					double value = Math.sin(Math.PI*progress);
					return Float.valueOf((float)value);
				}
			});
		} catch (GraphicsException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		blinkAnimation = null;
		super.finalize();
	}
	
	public void setAlpha(Float alpha) {
		setProperty("alpha", alpha);
	}
	
	public void startBlink() {
		AnimationManager.registerAnimation(blinkAnimation);
	}
	
	public void stopBlink() {
		AnimationManager.removeTarget(this);
		setProperty("alpha", 1.0f);
	}
	                          
}
